# Kerberos login for GNOME Shell

This extension adds a menu item to the GNOME Shell quick settings menu, listing
the Kerberos accounts that the user has enrolled into GNOME Online Accounts.
The menu shows the status of Kerberos accounts and lets the user refresh
the Kerberos ticket by clicking on the account.

## Installing

The extension can be installed from
https://extensions.gnome.org/extension/1165/kerberos-login/

## Supported GNOME Shell versions

The current version of the extension supports GNOME Shell 45 only.
Older versions are available on extensions.gnome.org for GNOME Shell
versions 3.22.2 and newer.

## License

The Kerberos login extension is released under the GNU General Public License,
version 2 or (at your option) any later version.
