// copyright (C) 2017 Paolo Bonzini
// SPDX-License-Identifier: GPL-2.0-or-later

import {domain as gettextDomain} from 'gettext';

import Gio from 'gi://Gio';
import GLib from 'gi://GLib';
import GObject from 'gi://GObject';

import {Extension} from 'resource:///org/gnome/shell/extensions/extension.js';
import * as ObjectManager from 'resource:///org/gnome/shell/misc/objectManager.js';
import * as Util from 'resource:///org/gnome/shell/misc/util.js';
import * as Signals from 'resource:///org/gnome/shell/misc/signals.js';

import * as Main from 'resource:///org/gnome/shell/ui/main.js';
import * as PopupMenu from 'resource:///org/gnome/shell/ui/popupMenu.js';
import * as QuickSettings from 'resource:///org/gnome/shell/ui/quickSettings.js';

const _ = {
    oa: gettextDomain('gnome-online-accounts').gettext,
    cc: gettextDomain('gnome-control-center').gettext,
};

const AccountIface = '<node>						\
  <interface name="org.gnome.OnlineAccounts.Account">			\
    <property name="AttentionNeeded" type="b" access="read"/>		\
    <property name="Id" type="s" access="read"/>			\
    <property name="PresentationIdentity" type="s" access="read"/>	\
    <property name="TicketingDisabled" type="b" access="readwrite"/>	\
    <method name="EnsureCredentials">					\
      <arg name="expires_in" type="i" direction="out"/>			\
    </method>								\
  </interface>								\
</node>';

const TicketingIface = '<node>						\
  <interface name="org.gnome.OnlineAccounts.Ticketing">			\
    <method name="GetTicket"/>						\
  </interface>								\
</node>';


let _accountManager = null;

function getAccountManager() {
    if (_accountManager == null)
        _accountManager = new AccountManager();

    return _accountManager;
}

class AccountManager extends Signals.EventEmitter {
    constructor() {
        super();
        this._accounts = {};
        this._objectManager = new ObjectManager.ObjectManager({ connection: Gio.DBus.session,
                                                                name: 'org.gnome.OnlineAccounts',
                                                                objectPath: '/org/gnome/OnlineAccounts',
                                                                knownInterfaces: [AccountIface, TicketingIface],
                                                                onLoaded: this._onLoaded.bind(this) });
    }

    _onLoaded() {
        let accounts = this._objectManager.getProxiesForInterface('org.gnome.OnlineAccounts.Account');

        for (let i = 0; i < accounts.length; i++)
            this._addAccount(accounts[i]);

        this._objectManager.connect('interface-added', (objectManager, interfaceName, proxy) => {
            if (interfaceName == 'org.gnome.OnlineAccounts.Account')
                this._addAccount(proxy);
        });

        this._objectManager.connect('interface-removed', (objectManager, interfaceName, proxy) => {
            if (interfaceName == 'org.gnome.OnlineAccounts.Account')
                this._removeAccount(proxy);
        });
    }

    _removeAccountByPath(objectPath) {
        let account = this._accounts[objectPath];
        delete this._accounts[objectPath];
        this.emit('account-removed', account);
    }

    _addAccount(account) {
        let objectPath = account.get_object_path();

        if (objectPath in this._accounts) {
            if (this._accounts[objectPath] == account) {
                return;
            }
            this._removeAccountByPath(objectPath);
        }
        if (!account.TicketingDisabled && this.getTicketingProxy(account) != null) {
            this._accounts[objectPath] = account;
            this.emit('account-added', account);
        }
    }

    _removeAccount(account) {
        let objectPath = account.get_object_path();

        if (this._accounts[objectPath] == account) {
            this._removeAccountByPath(objectPath);
        }
    }

    getTicketingProxy(account) {
        let objectPath = account.get_object_path();
        return this._objectManager.getProxy(objectPath, 'org.gnome.OnlineAccounts.Ticketing');
    }

    getAccounts() {
        let accounts = this._accounts;
        return Object.keys(accounts).map(k => {
            return accounts[k];
        });
    }

    hasAccounts() {
        return Object.keys(this._accounts).length > 0;
    }

    refreshAccounts() {
        for (let objectPath in this._accounts) {
            let account = this._accounts[objectPath];
            if (!account.AttentionNeeded) {
                account.EnsureCredentialsSync();
            }
        }
    }
}

class AccountUI {
    constructor(account) {
        this._account = account;
        this._ticketing = getAccountManager().getTicketingProxy(account);
        this._buildUI();
    }

    _clicked() {
        if (this._account.AttentionNeeded) {
            this._ticketing.GetTicketRemote();
        }
    }

    _buildUI() {
        this.menuItem = new PopupMenu.PopupMenuItem(this._account.PresentationIdentity, false);
        this.menuItem.connect('activate', this._clicked.bind(this));
    }

    destroy() {
        this.menuItem.destroy();
    }

    get_object_path() {
        return this._account.get_object_path();
    }

    sync() {
        this.menuItem.enabled = this._account.AttentionNeeded;
        this.menuItem.setOrnament(this._account.AttentionNeeded
            ? PopupMenu.Ornament.NONE
            : PopupMenu.Ornament.CHECK);
    }
}

// TODO: when AttentionNeeded state changes, highlight the menu?

var AccountMenuToggle = GObject.registerClass(
class AccountMenuToggle extends QuickSettings.QuickMenuToggle {
    _init(params) {
        this._extensionPath = Extension.lookupByURL(import.meta.url).path;
        this._menuIcon = Gio.icon_new_for_string(this._extensionPath + `/icons/system-menu-kerberos-22x22.png`);
        super._init({
            title: _.oa('Enterprise Login (Kerberos)'),
            gicon: this._menuIcon,
            ...params
        });

        this._accountUIs = {};
        this._sortedAccountUIs = [];
        this._buildUI();

        this._accountAdded = getAccountManager().connect('account-added', this._addAccount.bind(this));
        this._accountRemoved = getAccountManager().connect('account-removed', this._removeAccount.bind(this));

        let accounts = getAccountManager().getAccounts();
        for (let i in accounts) {
            this._addAccount(getAccountManager(), accounts[i]);
        }
    }

    _buildUI() {
        this.menu.setHeader(this._menuIcon, _.oa('Enterprise Login (Kerberos)'))

        this._accountSection = new PopupMenu.PopupMenuSection();
        this.menu.addMenuItem(this._accountSection);
        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());
        this.menu.addSettingsAction(_.cc('Online Accounts'), 'gnome-online-accounts-panel.desktop');

        this.connect('clicked', () => this.menu.open());
        this.menu.connect('open-state-changed', this._subMenuOpenStateChanged.bind(this));
    }

    _addAccountUI(accountUI) {
        let objectPath = accountUI.get_object_path();
        this._accountUIs[objectPath] = accountUI;
        let pos = Util.insertSorted(this._sortedAccountUIs, accountUI, (one, two) => {
            return GLib.utf8_collate(one._account.PresentationIdentity, two._account.PresentationIdentity);
        });
        this._accountSection.addMenuItem(accountUI.menuItem, pos);
    }

    _removeAccountUI(accountUI) {
        let objectPath = accountUI.get_object_path();
        let pos = this._sortedAccountUIs.indexOf(accountUI);
        if (pos >= 0) {
            this._sortedAccountUIs.splice(pos, 1);
        }
        accountUI.destroy();
        delete this._accountUIs[objectPath];
    }

    _sync() {
        this.menuEnabled = getAccountManager().hasAccounts();
    }

    destroy() {
        getAccountManager().disconnect(this._accountAdded);
        getAccountManager().disconnect(this._accountRemoved);

        while (this._sortedAccountUIs.length > 0) {
            this._removeAccountUI(this._sortedAccountUIs.pop());
        }
        this._accountSection.destroy();
        this.menu.destroy();
        super.destroy();
    }

    // Signal handlers

    _addAccount(accountManager, account) {
        this._addAccountUI(new AccountUI(account));
        this._sync();
    }

    _removeAccount(accountManager, account) {
        let objectPath = account.get_object_path();
        if (objectPath in this._accountUIs) {
            this._removeAccountUI(this._accountUIs[objectPath]);
        }
        this._sync();
    }

    _subMenuOpenStateChanged(menu, open) {
        if (!open) {
            return;
        }

        getAccountManager().refreshAccounts();
        for (let objectPath in this._accountUIs) {
            this._accountUIs[objectPath].sync();
        }
    }
})

var AccountIndicator = GObject.registerClass(
class AccountIndicator extends QuickSettings.SystemIndicator {
    _init() {
        super._init();

        this.quickSettingsItems.push(new AccountMenuToggle());
    }

    destroy() {
        while (this.quickSettingsItems.length > 0) {
            this.quickSettingsItems.pop().destroy();
        }
}
})

export default class KInitExtension extends Extension {
    enable() {
        let quickSettings = Main.panel.statusArea.quickSettings;

        this._accountIndicator = new AccountIndicator();
        quickSettings.addExternalIndicator(this._accountIndicator);
    }

    disable() {
        let quickSettings = Main.panel.statusArea.quickSettings;

        quickSettings._indicators.remove_child(this._accountIndicator);
        this._accountIndicator.destroy();
        this._accountIndicator = null;
    }
}
